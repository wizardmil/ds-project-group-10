import matplotlib.pyplot as plt
import math, pandas

def order(lines):
    structured = []
    for line in lines:
        l = line.split(",")

        x = float(l[0])
        y = float(l[1])

        structured += [x-y if x>y else y-x]

    return structured

def get(name):
    with open(name, "r") as f:
        lines = f.readlines()
        data = order(lines)
    return data

def mean(lst):
    return sum(lst)/len(lst)

def bucket(data):
    size =6
    _min = int(min(data))
    _max = int(max(data))
    _range = int((_max - _min)/size)
    series = pandas.DataFrame(data, index=list(range(len(data))), columns =['RN']) 
    intervals = []
    for i in range(_min, _max, _range):
        intervals += [("%d-%d"%(i, i+_range), len(series[(series.RN >= i)&(series.RN < i+_range)].RN))]
    return intervals

# 0.5 hertz
half = get("results_1.txt")

# 1 hertz
one = get("results_2.txt")

# 2 hertz
two = get("results_3.txt")

# 100 hertz
hundred = get("results_4.txt")

h_mean = mean(half)
o_mean = mean(one)
t_mean = mean(two)
u_mean = mean(hundred)

# plt.plot(range(len(one)), one, label="0.5Hz")
# plt.plot(range(len(two)), two, label="1Hz")
# plt.plot(range(len(half)), half, label="2Hz")
# plt.plot(range(len(hundred)), hundred, label="100Hz")
intervals = bucket(one)
plt.bar(range(len(intervals)), [i for x,i in intervals])
plt.xticks(range(len(intervals)), [x for x,i in intervals], rotation=90)
plt.legend()
plt.show()
