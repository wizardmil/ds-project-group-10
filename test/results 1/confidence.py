import matplotlib.pyplot as plt
import math, pandas

def order(lines, substract=None):
    structured = []
    for line in lines:
        l = line.split(",")

        x = float(l[0])
        y = float(l[1])

        result = (x-y)/60 if x>y else (y-x)/60

        if substract!=None:
            result-=substract

        structured += [result]
    return structured

def get(name, substract=None):
    with open(name, "r") as f:
        lines = f.readlines()
        data = order(lines, substract)
    return data

def mean(lst):
    return sum(lst)/len(lst)

def bucket(data):
    size =6
    _min = int(min(data))
    _max = int(max(data))
    _range = int((_max - _min)/size)
    series = pandas.DataFrame(data, index=list(range(len(data))), columns =['RN']) 
    intervals = []
    for i in range(_min, _max, _range):
        intervals += [("%d-%d"%(i, i+_range), len(series[(series.RN >= i)&(series.RN < i+_range)].RN))]
    return intervals

# 0.5 hertz
one = get("results_1.txt") + get("results_2.txt") + get("results_3.txt")
ten = get("results_5.txt", max(one)) + get("results_6.txt", max(one)) + get("results_7.txt", max(one))


plt.plot(range(len(one)), one, label="1000  Hz")
plt.plot(range(len(ten)), ten, label="10000 Hz")

# intervals = bucket(one)
# plt.bar(range(len(intervals)), [i for x,i in intervals])
# plt.xticks(range(len(intervals)), [x for x,i in intervals], rotation=90)
plt.legend()
plt.show()
