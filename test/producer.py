from kclient import KafkaClient
import json, asyncio
from time import sleep
import time

client = KafkaClient('192.168.43.143:9092', 'main.group')

loop = asyncio.new_event_loop()
asyncio.set_event_loop(loop)

print("Test Info:")
print(" * Results are received in consumer (consumer must be deployed).")
print(" * Assumes hdfs contains 1 month worth of data.")
print(" * Waits 15 seconds between each test.")
print("")
print("Starting in 5 seconds ...")
print("")

# sleep(5)

# query = '{"STAT":"mean,variance", "from":"2001-01-01", "to":"2001-01-07", "TOPIC":"out.topic", "SELECT":"district, arrest, SUM(count)", "GROUPBY":"district, arrest"}'
# print("0] Testing query: " + query)
# loop.run_until_complete(client.send_message('in.topic', query))
# print(" ")

# sleep(10)

# query = '{"from":"2001-01-01", "to":"2001-01-07", "TOPIC":"out.topic", "SELECT":"district, date, SUM(count)", "GROUPBY":"district, date"}'
# print("0.1] Testing query: " + query)
# loop.run_until_complete(client.send_message('in.topic', query))
# print(" ")

# sleep(10)

for i in range(50):
    query = '{"time":' + str(time.time()) + ', "from":"2001-01-01", "to":"2001-01-02"}'
    print("testing 1000 hz")
    loop.run_until_complete(client.send_message('in.topic', query))
    sleep(0.001)

print("sending fin")
sleep(30)
query = '{"time":"fin"' + ', "from":"2001-01-01", "to":"2001-01-02"}'
print("finishing test")
loop.run_until_complete(client.send_message('in.topic', query))
print(" ")
sleep(600)
# 
for i in range(50):
    query = '{"time":' + str(time.time()) + ', "from":"2001-01-01", "to":"2001-01-02"}'
    print("testing 10000 hz")
    loop.run_until_complete(client.send_message('in.topic', query))
    sleep(0.0001)

print("sending fin")
sleep(30)
query = '{"time":"fin"' + ', "from":"2001-01-01", "to":"2001-01-02"}'
print("finishing test")
loop.run_until_complete(client.send_message('in.topic', query))
print(" ")
sleep(10)

# for i in range(50):
#     query = '{"time":' + str(time.time()) + ', "from":"2001-01-01", "to":"2001-01-02"}'
#     print("testing 2 hz")
#     loop.run_until_complete(client.send_message('in.topic', query))
#     sleep(0.5)

# print("sending fin")
# sleep(30)
# query = '{"time":"fin"' + ', "from":"2001-01-01", "to":"2001-01-02"}'
# print("finishing test")
# loop.run_until_complete(client.send_message('in.topic', query))
# print(" ")
# sleep(600)

# for i in range(50):
#     query = '{"time":' + str(time.time()) + ', "from":"2001-01-01", "to":"2001-01-02"}'
#     print("testing 100 hz")
#     loop.run_until_complete(client.send_message('in.topic', query))
#     sleep(0.01)    

# print("sending fin")
# sleep(30)
# query = '{"time":"fin"' + ', "from":"2001-01-01", "to":"2001-01-02"}'
# print("finishing test")
# loop.run_until_complete(client.send_message('in.topic', query))
# print(" ")
# sleep(600)

# print("1] Getting data for 9 days (should result in dataset)")
# query = '{"STAT":"mean", "from":"2001-01-01", "to":"2001-01-02"}'
# loop.run_until_complete(client.send_message('in.topic', query))
# print("waiting 5 seconds ....")
# print(" ")

# sleep(5)

# print("2] Testing error message (should result in error response)")
# query = '{"from":"2001-01-01", "to":"2050-01-06"}'
# loop.run_until_complete(client.send_message('in.topic', query))
# print(" ")

# sleep(1)

print("")
print("Test deployed.")