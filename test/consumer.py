from kclient import KafkaClient
import json, asyncio

client = KafkaClient('192.168.43.143:9092', 'main.group')

loop = asyncio.new_event_loop()
asyncio.set_event_loop(loop)

loop.run_until_complete(client.subscribe('out.topic'))
loop.run_forever()