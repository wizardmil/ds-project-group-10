# Crimes of Chicago
This project was conducted under the facilities of the University of Southern Denmark during the *software engineering, data science course 2019, 1. semester masters.* The course was taught by Dr. Fisayo Caleb Sangogboye and Dr. Jakob Hviid.

## How to
See requirements in the *Dependencies* section.

1. Start Docker Desktop. 
2. Enter the `project` folder. 
3. Execute `run.cmd`in `/kafka/` folder. 
4. Execute the `init.cmd` in `/hadoop/` folder. 
5. Execute the `run.cmd` in `/hadoop/` folder. <br>
    5.01 **Optional:** Test the setup using by running the `consumer.py` and then `producer.py` separately from the `/test/` folder.
6. Start the `Webserver`by executing the `run.py`script located in the `web` folder.
7. Visit `localhost:8000` in a browser (this has been tested with Chrome).

## Query
The current version of this setup receives queries through the `in.topic` topic. Queries are formatted as strings, containing a dictionary with the keys **from** and **to**.
These keys both contain dates formatted as such; `%Y-%m-%d`. The hadoop engine will return data stored, within the given period.

```python
query = '{"from":"2001-01-01", "to":"2001-01-06"}'
```

Data returned are formatted as listed beneath. Arrested as well as not_arrested contains; *date*, *district*, *type of crime* and *count*. The count indicates the number of similar incidents during the same date, and in the same district. 

```python
{"status" : "success", "arrested" : [[...], ["2003-04-08", 13, "ROBBERY", 654], [...]], "not_arrested" : []}
```

If a **to** date given in a query is larger than the processed data on the hdfs, the data returned will contain a key; **processed** which includes the date of the newest processed data stored in the dfs.
```python
{"status" : "error", "processed" : "2019-03-14"}
```

## Dependencies

The project runs using:
1. Docker (Docker Desktop): This includes the HDFS, the engine and the Kafka cluster.<br>
    1.1 It is advised to run Docker Desktop with >2 GB of RAM.<br>
2. Python 3.7.4 with the following packages:<br>
    2.01 **aipkafka**<br>
    2.02 **asyncio**<br>
    2.03 **base64**<br>
    2.04 **jinja2**<br>
    2.05 **json**<br>
    2.06 **aiohttp**<br>
    2.07 **aiohttp_session**<br>
    2.08 **cryptography**<br>
    2.09 **threading**<br>
    2.10 **aiohttp_jinja2**.<br>

Install by running `python -m pip install <module>` 
## Authors

Mikkel Bjerg, mibje15@student.sdu.dk <br>
Arian Ebrahimi, arebr15@student.sdu.dk <br>
Emil Stubbe Kolvig-Raun, emstu15@student.sdu.dk <br>