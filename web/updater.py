from threading import Thread
from storage import db
from time import sleep
from kafkaclient.kafka_client import KafkaClient
import asyncio, json
from kafkaclient.kafka_client import InterfaceResponseHandler
import datetime

class DataFetcher(Thread, InterfaceResponseHandler):

    def __init__(self, _global, max_date):
        self._global = _global
        self.starting = max_date
        self.client = KafkaClient('192.168.43.143:9092', 'main.group')
        
        self.arrested = db.DatabaseConnection("arrested")
        self.n_arrested = db.DatabaseConnection("n_arrested")

        self.loop = asyncio.new_event_loop()
        asyncio.set_event_loop(self.loop)

        self._global.start = str(self.starting)
        self.frm = datetime.datetime.strptime(str(self.starting), '%Y-%m-%d')
        self.to = self.frm + datetime.timedelta(days=1)

        self.sleep = False

        Thread.__init__(self)

    # fetch from the databases
    def run(self):
        print("")
        # while True:
        #     if self.sleep:
        #         sleep(30)
        #         self.sleep=False

        #     query = '{"from":"'+str(self.frm.strftime("%Y-%m-%d"))+'", "to":"'+str(self.to.strftime("%Y-%m-%d"))+'"}'

        #     self.loop.run_until_complete(self.client.send_message('in.topic', query))

        #     self._global.until = str(self.to.strftime("%Y-%m-%d"))

        #     print(query)
        #     sleep(10)

    def handle(self, message):
        # receives kafka messages through here
        data = json.loads(message.value.decode('utf-8'))
        data = json.loads(data)

        if data["status"]=="success":

            arrested = data["arrested"]
            n_arrested = data["not_arrested"]

            """
            +--------+--------+--------+-------+
            |  date  |district|  type  | count |
            +--------+--------+--------+-------+
            """        

            # for row in arrested:
            #     self.arrested.insert(row[0], row[1], row[2], row[3])
            
            # for row in n_arrested:
            #     self.n_arrested.insert(row[0], row[1], row[2], row[3])

            self.frm += datetime.timedelta(days=1)
            self.to += datetime.timedelta(days=1)
        else:
            self.sleep = True    
