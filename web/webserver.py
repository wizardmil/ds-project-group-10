#!/usr/bin/env python3.7.4

from kafkaclient.kafka_client import KafkaClient
import asyncio, base64, jinja2, os, json
from aiohttp import web
from aiohttp_session import setup
from aiohttp_session.cookie_storage import EncryptedCookieStorage
from cryptography.fernet import Fernet
from aiohttp_jinja2 import setup as jinja_setup

# InterfaceResponseHandler 
class WebServer():
    """
    This is the Webserver.
    The webserver exposes the webinterface and 
    establishes a REST-like interface using aiohttp.
    """
    def __init__(self, routes, fetcher):
        self.application = web.Application()
        self.client = KafkaClient('192.168.43.143:9092', 'main.group')
        self.host = "localhost"                     # the host which the webserver is running on
        self.port = 8000                            # and the port to access it
        self.routes = routes                        # web route handler

        self.fetcher = fetcher

        self.loop = asyncio.new_event_loop()

    def run(self):

        asyncio.set_event_loop(self.loop)

        # Setting up symmetric cookie encryption using  
        # Fernet that guarantees that a message encrypted
        # using it cannot be manipulated or read without the key
        setup(self.application, EncryptedCookieStorage(base64.urlsafe_b64decode(Fernet.generate_key())))

        # Setting up the template engine and loading 
        # any and every content from the html folder
        jinja_setup(self.application, loader=jinja2.FileSystemLoader(os.path.join((os.path.split(__file__))[0],"public")))
        
        # Adding the different routes to the webserver
        # anything from pages must be added to (path to page, handler)
        self.application.add_routes(self.routes)  

        # We have to add the javascript files statically
        # to be able to reference them. Any JS file is now 
        # referenced to using /static/
        self.application.router.add_static('/static', path='./public/static/', name='static')

        # Running the webserver using the host
        # and port configurations, and the web application
        server = self.loop.create_server(self.application.make_handler(), self.host, self.port)

        print('========== Data Science Webserver running on: http://%s:%d/ ==========' % (self.host, self.port))

        # using asyncio to gather the processes as tasks and run 
        # them separately in foreground and background
        try:
            self.loop.run_until_complete(asyncio.gather(server, self.client.subscribe(['out.topic'], self.fetcher)))
            self.loop.run_forever()
        except KeyboardInterrupt:
            print("=================== Server Shutting Down... ====================")
            exit(0)
        except Exception as e:
            print(e)
            exit(0)


        