var _chart = null;

function showChart(district, crime_type, chart_type, arrested_data, not_arrested_data) {
    var _labels = new Array(19);
    var year = 2001;
    for(var i = 0; i < 19; i += 1) {
        _labels[i] = year + i;
    }

    var _data = new Array(19);
    var _dataAlt = new Array(19);
    var _changes = new Map();

    switch(chart_type) {
        case "least_to_most_solved":
            if(district == "*") {
                for(var i = 0; i < arrested_data.length; i += 1) {
                    if(_data[arrested_data[i][0] - 2001] == undefined) {
                        _data[arrested_data[i][0] - 2001] = parseInt(arrested_data[i][2]);
                    }
                    else
                    {
                        _data[arrested_data[i][0] - 2001] = parseInt(_data[arrested_data[i][0] - 2001]) + parseInt(arrested_data[i][2]);
                    }
                }
            }
            else
            {
                for(var i = 0; i < arrested_data.length; i += 1) {
                    if(arrested_data[i][1] == district) { 
                        if(_data[arrested_data[i][0] - 2001] == undefined) {
                            _data[arrested_data[i][0] - 2001] = parseInt(arrested_data[i][2]);
                        }
                    }
                }
            }
            
            break;

        case "percentage_solved":
            if(district == "*") {
                for(var i = 0; i < arrested_data.length; i += 1) {
                    if(_data[arrested_data[i][0] - 2001] == undefined) {
                        _data[arrested_data[i][0] - 2001] = parseInt(arrested_data[i][2]);
                    }
                    else
                    {
                        _data[arrested_data[i][0] - 2001] = parseInt(_data[arrested_data[i][0] - 2001]) + parseInt(arrested_data[i][2]);
                    }
                }
    
                for(var i = 0; i < not_arrested_data.length; i += 1) {
                    if(not_arrested_data[i] == undefined) {
                        continue;
                    }

                    if(_dataAlt[not_arrested_data[i][0] - 2001] == undefined) {
                        _dataAlt[not_arrested_data[i][0] - 2001] = parseInt(not_arrested_data[i][2]);
                    }
                    else
                    {
                        _dataAlt[not_arrested_data[i][0] - 2001] = parseInt(_dataAlt[not_arrested_data[i][0] - 2001]) + parseInt(not_arrested_data[i][2]);
                    }
                }
            }
            else
            {
                for(var i = 0; i < arrested_data.length; i += 1) {
                    if(arrested_data[i][1] == district) {
                        if(_data[arrested_data[i][0] - 2001] == undefined) {
                            _data[arrested_data[i][0] - 2001] = parseInt(arrested_data[i][2]);
                        }
                    }
                }
    
                for(var i = 0; i < not_arrested_data.length; i += 1) {
                    if(not_arrested_data[i] == undefined) {
                        continue;
                    }

                    if(not_arrested_data[i][1] == district) {
                        _dataAlt[not_arrested_data[i][0] - 2001] = parseInt(not_arrested_data[i][2]);
                    }
                }
            }
            
            break;

        case "not_solved":
            if(district == "*") {
                for(var i = 0; i < not_arrested_data.length; i += 1) {
                    if(_data[not_arrested_data[i][0] - 2001] == undefined) {
                        _data[not_arrested_data[i][0] - 2001] = parseInt(not_arrested_data[i][2]);
                    }
                    else
                    {
                        _data[not_arrested_data[i][0] - 2001] = parseInt(_data[not_arrested_data[i][0] - 2001]) + parseInt(not_arrested_data[i][2]);
                    }
                }
            }
            else
            {
                for(var i = 0; i < not_arrested_data.length; i += 1) {
                    if(not_arrested_data[i][1] == district) {
                        _data[not_arrested_data[i][0] - 2001] = parseInt(not_arrested_data[i][2]);
                    }
                }
            }
            
            break;

        case "least_to_most_occurences":
            for(var i = 0; i < arrested_data.length; i += 1) {
                if(district == "*") {
                    if(_data[arrested_data[i][0] - 2001] == undefined) {
                        _data[arrested_data[i][0] - 2001] = parseInt(arrested_data[i][2]);
                    }
                    else
                    {
                        _data[arrested_data[i][0] - 2001] = parseInt(_data[arrested_data[i][0] - 2001]) + parseInt(arrested_data[i][2]);
                    }
                }
                else
                {
                    if(arrested_data[i][1] == district) {
                        if(_data[arrested_data[i][0] - 2001] == undefined) {
                            _data[arrested_data[i][0] - 2001] = parseInt(arrested_data[i][2]);
                        }
                        else
                        {
                            _data[arrested_data[i][0] - 2001] = parseInt(_data[arrested_data[i][0] - 2001]) + parseInt(arrested_data[i][2]);
                        }
                    }
                }
            }

            for(var i = 0; i < not_arrested_data.length; i += 1) {
                if(district == "*") {
                    if(_data[not_arrested_data[i][0] - 2001] == undefined) {
                        _data[not_arrested_data[i][0] - 2001] = parseInt(not_arrested_data[i][2]);
                    }
                    else
                    {
                        _data[not_arrested_data[i][0] - 2001] = parseInt(_data[not_arrested_data[i][0] - 2001]) + parseInt(not_arrested_data[i][2]);
                    }
                }
                else
                {
                    if(not_arrested_data[i][1] == district) {
                        if(_data[not_arrested_data[i][0] - 2001] == undefined) {
                            _data[not_arrested_data[i][0] - 2001] = parseInt(not_arrested_data[i][2]);
                        }
                        else
                        {
                            _data[not_arrested_data[i][0] - 2001] = parseInt(_data[not_arrested_data[i][0] - 2001]) + parseInt(not_arrested_data[i][2]);
                        }
                    }
                }
            }
            break;
    }

    for(var i = 0; i < _data.length; i += 1) {
        if(i == 0) {
            _changes.set(i, "-");
        } 
        else 
        {
            if(_data[i] == undefined || _data[i - 1] == undefined) {
                _changes.set(i, "-");
            } 
            else 
            {
                var change = parseInt(_data[i]) / parseInt(_data[i - 1]);
                var sign = "+";

                if(change < 1) {
                    change = 1 - change;
                    sign = "-";
                } else {
                    change = change - 1;
                }

                change *= 100;

                if(change > 0 && change < 1 || change > -1 && change < 0) {
                    change = "<1";
                }
                else {
                    change = Math.trunc(change);
                }

                _changes.set(i, sign + "" + change.toString() + "%");
            }
        }
    }

    var ctx = document.getElementById('chart').getContext('2d');

    var legendEnd = "";
    var barColor = "#000";
    switch(chart_type) {
        case "least_to_most_occurences":
            barColor = "#5a9bd4";
            legendEnd = "occurences";
            break;

        case "not_solved":
            barColor = "#ce7058";
            legendEnd = "not solved";
            break;

        case "least_to_most_solved":
            barColor = "#5a9bd4";
            legendEnd = "solved";
            break;
    }

    if(_chart != null) {
        _chart.destroy();
    }

    var _formatterIndex = 0;
    if(chart_type == "percentage_solved") {
        _chart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: _labels,
                datasets: [{
                    label: 'Cases of ' + String(crime_type).toLowerCase() + ' solved',
                    backgroundColor: "#5a9bd4",
                    data: _data
                }, {
                    label: 'Cases of ' + String(crime_type).toLowerCase() + ' not solved',
                    backgroundColor: "#ce7058",
                    data: _dataAlt
                }]
            },
            options: {
                plugins: {
                    datalabels: {
                        color: '#000',
                        formatter: (value, ctx) => {
                            var datasetSolved = ctx.chart.data.datasets[0].data;
                            var datasetNotSolved = ctx.chart.data.datasets[1].data;

                            var sum = (datasetSolved[ctx.dataIndex] + datasetNotSolved[ctx.dataIndex]);

                            if(isNaN(sum)) {
                                return "100%";
                            }

                            var result = Math.trunc((value / sum) * 100);

                            if(result < 1) {
                                return "<1%";
                            }

                            if(result < 99 && result > 1) {
                                var currentPerc = Math.trunc((value / sum) * 100);
                                var otherPerc = Math.trunc(((sum - value) / sum) * 100);
                                var totalPerc = currentPerc + otherPerc;

                                if(currentPerc > otherPerc) {
                                    currentPerc += (100 - totalPerc);
                                }

                                return (currentPerc + "%");
                            }

                            return (result + "%");
                        }
                    }
                },
                legend: {
                    labels: {
                        fontColor: "#000"
                    }
                },
                scales: {
                    xAxes: [{
                        stacked: true,
                        fontColor: "#000"
                    }],
                    yAxes: [{
                        fontColor: "#000",
                        stacked: true,
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        }); 
    }
    else
    {
        _chart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: _labels,
                datasets: [{
                    label: 'Cases of ' + String(crime_type).toLowerCase() + ' ' + legendEnd,
                    backgroundColor: barColor,
                    data: _data
                }]
            },
            options: {
                "animation": {
                    "duration": 0,
                        "onComplete": function() {
                            var chartInstance = this.chart,
                            ctx = chartInstance.ctx;
                            
                            ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'bottom';
                            
                            this.data.datasets.forEach(function(dataset, i) {
                            var meta = chartInstance.controller.getDatasetMeta(i);
                            meta.data.forEach(function(bar, index) {
                              var data = _changes.get(index);
                              ctx.fillText(data, bar._model.x, bar._model.y - 5);
                            });
                        });
                    }
                },
                plugins: {
                    datalabels: {
                        display: false
                    },
                },
                legend: {
                    labels: {
                        fontColor: "#000"
                    }
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        }); 
    }
};

Chart.Legend.prototype.afterFit = function() {
    this.height = this.height + 15;
};