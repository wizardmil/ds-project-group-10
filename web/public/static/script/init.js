var normalization = "off";
var mode = "percentage_solved";
var dataParam = "ALL CRIMES";
var fromDateParam = '2001-01-01';
var toDateParam = '2100-01-01';

var repeater = setTimeout(getUpdate, 2);
var update_display = setTimeout(updateView, 1);

var updated = true;
var until = null;

var mapView = true;

function init() {
    //defaultBoundary = document.getElementById("defaultBoundary");
    //defaultBoundary.checked = true;
    //var boundaryRadios = document.getElementsByName("boundaryRadio");
    //for(i = 0; i < boundaryRadios.length; i += 1) {
    //    boundaryRadios[i].onclick = function() {
    //        setBoundary(this.value);
    //    }
    //}

    defaultView = document.getElementById("defaultView");
    defaultView.checked = true;

    defaultMode = document.getElementById("defaultMode");
    defaultMode.checked = true;

    defaultData = document.getElementById("defaultData");
    defaultData.checked = true;

    var viewRadios = document.getElementsByName("viewRadio");
    for(i = 0; i < viewRadios.length; i += 1) {
        // console.log(viewRadios[i].value);
        switch(viewRadios[i].value) {
            case "heatmap":
                viewRadios[i].onclick = function() {
                    mapView = true;
                    document.getElementById("heatmapContainer").style.display = "block";
                    document.getElementById("chartContainer").style.display = "none";
                    updated = true;
                    updateView();
                }
                break;

            case "chart":
                viewRadios[i].onclick = function() {
                    mapView = false;
                    document.getElementById("heatmapContainer").style.display = "none";
                    document.getElementById("chartContainer").style.display = "block";
                    updated = true;
                    updateView();
                }
                break;
        }
    }

    var modal = document.getElementById("dataModal");
    var btn = document.getElementById("modalBtn");
    var span = document.getElementsByClassName("close")[0];

    btn.onclick = function() {
        modal.style.display = "block";
    }

    span.onclick = function() {
        modal.style.display = "none";
    }

    window.onclick = function(event) {
        if(event.target == modal) {
            modal.style.display = "none";
        } 
    }

    var fromDate = document.getElementById("fromDate");
    var toDate = document.getElementById("toDate");

    fromDate.value = '2001-01-01'
    toDate.value = '2019-01-01'
    fromDateParam = fromDate.value;
    toDateParam = toDate.value;

    $("input[name='dataRadio']").change(function() {
        updated = true;
        dataParam = this.value;
        document.getElementById("showingTxt").innerHTML = this.value;
        updateView();
    });

    $("input[name='modeRadio']").change(function() {
        updated = true;
        mode = this.value;
        setLegend(this.value);
        updateView();
    });

    $(".dateSelector").change(function() {
        updated = true;
        fromDateParam = fromData.value;
        toDateParam = toDate.value;
        updateView();
    });

    $("#districtSelector").change(function() {
        updated = true;
        updateView();
        // Probably better to just reuse data from last request rather than query again. 
        // This would avoid long wait time between same view for different districts
    });

    updateView();
}

function updateView() {
    if (updated == true){
    if(mapView == true) {
        $.getJSON('http://localhost:8000/fetch/arrested/' + dataParam + "/" + fromDateParam + "/" + toDateParam, function(arrested_data) {
            $.getJSON('http://localhost:8000/fetch/n_arrested/' + dataParam + "/" + fromDateParam + "/" + toDateParam, function(not_arrested_data) {
                var combinedJSON = [];
    
                for(i = 0; i < arrested_data.length; i++) {
                        var arrested_index = arrested_data[i][0];
                        var arrested_value = arrested_data[i][1];
                        
                        var not_arrested_value = 0;
    
                        try {
                            not_arrested_value = not_arrested_data[i][1];
                        }
                      catch (e) {
                          //ignore
                          not_arrested_value = 0;
                      }
                      
                      newEntry = {}
                      newEntry["dist_num"] = arrested_index;
    
                      switch(mode) {
                          case "percentage_solved":
                              newEntry["value"] = arrested_value / (arrested_value + not_arrested_value);
                            break;
                            
                            case "least_to_most_solved":
                                newEntry["value"] = arrested_value;
                                break;
                                
                                case "least_to_most_occurences":
                                    newEntry["value"] = arrested_value + not_arrested_value;
                                    break;
                                    case "not_solved":
                                        newEntry["value"] = not_arrested_value;
                                        break;
                    }
                    
                    combinedJSON.push(newEntry)
                } 
                
                setBoundaryWithData("districts", combinedJSON);
            });
        });
    }
    else 
    {
        $.getJSON('http://localhost:8000/fetch/arrested/' + dataParam, function(arrested_data) {
            $.getJSON('http://localhost:8000/fetch/n_arrested/' + dataParam, function(not_arrested_data) {
                var element = document.getElementById("districtSelector");
                showChart(element.options[element.selectedIndex].value, dataParam, mode, arrested_data, not_arrested_data);
            });   
        });
    }
} 

updated = false;
update_display = setTimeout(updateView, 15000);
}



function getUpdate() {
    $.getJSON('http://localhost:8000/date_marks', function(processed_info) {

        var to = processed_info[1];

        if (to!=until){
            until = to;
            updated = true;
        }
        document.getElementById("processed_until_text").innerHTML = "<h4>Including data from: "+  processed_info[0] + ", to: " + to + ".</h4>"
    });

    repeater = setTimeout(getUpdate, 3000);
}