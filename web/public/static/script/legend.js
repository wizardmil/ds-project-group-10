function setLegend(legend_identifier) {
    var legendTop = document.getElementsByClassName("legend_text_top")[0];
    var legendBottom = document.getElementsByClassName("legend_text_bottom")[0];

    switch(legend_identifier) {
        case "percentage_solved":
                legendTop.innerHTML = "Most percentage of cases solved"
                legendBottom.innerHTML = "Least percentage of cases solved"
            break;

        case "least_to_most_solved":
                legendTop.innerHTML = "Most cases solved"
                legendBottom.innerHTML = "Least cases solved"
            break;

        case "least_to_most_occurences":
                legendTop.innerHTML = "Most cases"
                legendBottom.innerHTML = "Least cases"
            break;
        
        case "not_solved":
                legendTop.innerHTML = "Most cases"
                legendBottom.innerHTML = "Least cases"
            break;

        default:
                legendTop.innerHTML = "High"
                legendBottom.innerHTML = "Low"
            break;
    }
}