var activeBoundary = "";

function setBoundary(identifier, dataset) {
    activeBoundary = identifier;
    var dataLocation = "";

    switch(identifier) {
        case "districts":
            dataLocation = "http://localhost:8000/static/boundaries/police_districts.geojson";
            break;

        case "beats":
            dataLocation = "http://localhost:8000/static/boundaries/police_beats.geojson";
            break;

        case "areas":
            dataLocation = "http://localhost:8000/static/boundaries/community_areas.geojson";
            break;

        case "wards":
            dataLocation = "http://localhost:8000/static/boundaries/wards.geojson";
            break;
    }

    if(map.getLayer('boundary-layer')) {
        map.removeLayer('boundary-layer');
        map.removeLayer('boundary-border');
    }

    if(map.getSource('boundaries')) {
        map.removeSource('boundaries');
    }

    map.addSource('boundaries', {
        'type':'geojson',
        'data':dataLocation
    });

    if(dataset == undefined) {
        map.addLayer({
          'id': 'boundary-layer',
          'type': 'fill',
          'source': 'boundaries',
          'paint': {
          'fill-color': 'rgba(30, 144, 255, 0.4)',
          'fill-outline-color': 'rgba(15, 72, 255, 255)'
          }
        });
    } 
    else 
    {
        switch(identifier) {
            case "districts":
                map.addLayer({
                    'id': 'boundary-layer',
                    'type': 'fill',
                    'source': 'boundaries',
                    'paint': {
                    'fill-color': {
                        property: 'dist_num',
                        type: 'categorical',
                        stops: [
                            ["1", dataset.getOrElse("1", "#000")],
                            ["2", dataset.getOrElse("2", "#000")],
                            ["3", dataset.getOrElse("3", "#000")],
                            ["4", dataset.getOrElse("4", "#000")],
                            ["5", dataset.getOrElse("5", "#000")],
                            ["6", dataset.getOrElse("6", "#000")],
                            ["7", dataset.getOrElse("7", "#000")],
                            ["8", dataset.getOrElse("8", "#000")],
                            ["9", dataset.getOrElse("9", "#000")],
                            ["10", dataset.getOrElse("10", "#000")],
                            ["11", dataset.getOrElse("11", "#000")],
                            ["12", dataset.getOrElse("12", "#000")],
                            ["14", dataset.getOrElse("14", "#000")],
                            ["15", dataset.getOrElse("15", "#000")],
                            ["16", dataset.getOrElse("16", "#000")],
                            ["17", dataset.getOrElse("17", "#000")],
                            ["18", dataset.getOrElse("18", "#000")],
                            ["19", dataset.getOrElse("19", "#000")],
                            ["20", dataset.getOrElse("20", "#000")],
                            ["22", dataset.getOrElse("22", "#000")],
                            ["24", dataset.getOrElse("24", "#000")],
                            ["25", dataset.getOrElse("25", "#000")],
                            ["31", dataset.getOrElse("31", "#000")]
                        ]
                    },
                    'fill-opacity': 0.5,
                    'fill-outline-color': 'rgba(15, 72, 255, 255)'
                    }
                  });
                break;

            case "beats":
                break;

            case "areas":
                break;
        }
    }
    
      map.addLayer({
        'id': 'boundary-border',
        'type': 'line',
        'source': 'boundaries',
        'paint': {
            //'line-color': 'rgba(24, 115, 204, 255)',
            'line-color': 'rgba(0, 0, 0, 255)', 
            'line-width': 1
        }
      });

      map.on('click', 'boundary-layer', function(e) {
          var output = "";

          switch(activeBoundary) {
              case "districts":
                  output = '<span>District label: </span>' + e.features[0].properties.dist_label + ' <br> <span>District number: </span> ' + e.features[0].properties.dist_num;
                  break;

              case "beats":
                  output = '<span>Beat number: </span>' + e.features[0].properties.beat_num + ' <br> <span>District number: </span> ' + e.features[0].properties.district + ' <br> <span>Sector number: </span> ' + e.features[0].properties.sector;
                  break;

              case "areas":
                  output = '<span>Community name: </span>' + e.features[0].properties.community + ' <br> <span>Community number: </span> ' + e.features[0].properties.area_numbe;
                  break;

              case "wards":
                  output = '<span>Ward number: </span>' + e.features[0].properties.ward;
                  break;
          }

        new mapboxgl.Popup()
        .setLngLat(e.lngLat)
        .setHTML(output)
        .addTo(map);
      });

      map.on('mouseenter', 'districts-layer', function() {
        map.getCanvas().style.cursor = 'pointer';
      });

      map.on('mouseleave', 'states-layer', function() {
        map.getCanvas().style.cursor = '';
      });
}

function setBoundaryWithData(boundary_identifier, dataset) {
    //setLegend(dataset_identifier);

    var min = 2147483648;
    var max = -2147483648;
    var diff = 0;
    var median = 0;

    for(var entry in dataset) {
        if(dataset[entry]["value"] < min) {
            min = dataset[entry]["value"];
        }

        if(dataset[entry]["value"] > max) {
            max = dataset[entry]["value"];
        }
    }

    diff = max - min;
    median = parseInt(((max - min) / 2)) + parseInt(min);

    var coloring = new Map();

    for(var entry in dataset) {
        var temp = median;

        temp = dataset[entry]["value"];

        var percentage = ((temp - min) / diff) * 100;

        coloring.set(dataset[entry]["dist_num"], toColor(percentage));
    }

    setBoundary("districts", coloring);
}

function toColor(percentage) {
	var r, g, b = 0;
	if(percentage < 50) {
		r = 255;
		g = Math.round(5.1 * percentage);
	}
	else {
		g = 255;
		r = Math.round(510 - 5.10 * percentage);
	}
	var h = r * 0x10000 + g * 0x100 + b * 0x1;
	return '#' + ('000000' + h.toString(16)).slice(-6);
}

Map.prototype.getOrElse = function(key, value) {
    return this.has(key) ? this.get(key) : value
  }