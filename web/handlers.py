#!/usr/bin/env python3.7.4

from threading import Thread
import json, asyncio, time
from threading import RLock
from aiohttp import web
from aiohttp_jinja2 import render_template
from storage import db

from updater import DataFetcher
from _global import GlobalVariables

# how to import kafka client (arrow down)
# from kafkaclient.kafka_client import KafkaClient


class ApplicationHandler:

    def __init__(self):  
        self._global = GlobalVariables()     
        self.routes = [
            web.get('/', self.main_page),
            web.get('/fetch/{table}/{type}/{from}/{to}', self.fetch_data),
            web.get('/fetch/{table}/{type}', self.fetch_data_simplified),
            web.get('/date_marks', self.get_date_marks)
        ]
        self.arrested = db.DatabaseConnection("arrested")
        self.arrested.create_metadata_table()
        self.n_arrested = db.DatabaseConnection("n_arrested")
        self.n_arrested.create_metadata_table()

        self.fetcher = DataFetcher(self._global, self.arrested.get_max_date())
        self.fetcher.start()

    # This is the configuration page for 
    # any subscription that is made through occure
    async def main_page(self, request):
        context = {}
        response = render_template("index.html", request, context)
        response.headers['Content-Language'] = 'en'
        return response 

    async def get_date_marks(self, request):
        return web.json_response([self._global.start, self._global.until])

    async def fetch_data(self, request):
        tableID = request.match_info.get('table', None)
        typeID = request.match_info.get('type', None)
        fromDATE = request.match_info.get('from', None)
        toDATE = request.match_info.get('to', None)

        if(tableID == "arrested"):
            if(typeID == "ALL CRIMES"):
                return web.json_response(self.arrested.query("SELECT district, SUM(count) FROM arrested WHERE date(date) BETWEEN date(\"" + str(fromDATE) + "\") AND date(\"" + str(toDATE) + "\") group by district"))
            else:
                return web.json_response(self.arrested.query("SELECT district, SUM(count) FROM arrested WHERE primary_type == \"" + str(typeID) + "\" AND date(date) BETWEEN date(\"" + str(fromDATE) + "\") AND date(\"" + str(toDATE) + "\") group by district"))
        elif(tableID == "n_arrested"):
            if(typeID == "ALL CRIMES"):
                return web.json_response(self.n_arrested.query("SELECT district, SUM(count) FROM n_arrested WHERE date(date) BETWEEN date(\"" + str(fromDATE) + "\") AND date(\"" + str(toDATE) + "\") group by district"))
            else:
                return web.json_response(self.n_arrested.query("SELECT district, SUM(count) FROM n_arrested WHERE primary_type == \"" + str(typeID) + "\" AND date(date) BETWEEN date(\"" + str(fromDATE) + "\") AND date(\"" + str(toDATE) + "\") group by district"))


    async def fetch_data_simplified(self, request):
        typeID = request.match_info.get('type', None)
        tableID = request.match_info.get('table', None)

        if(tableID == "arrested"):
            if(typeID == "ALL CRIMES"):
                return web.json_response(self.arrested.query("SELECT strftime('%Y', date) AS 'year', district, SUM(count) FROM arrested group by district, year"))
            else:
                return web.json_response(self.arrested.query("SELECT strftime('%Y', date) AS 'year', district, SUM(count) FROM arrested WHERE primary_type == \"" + str(typeID) + "\" group by district, year"))
        elif(tableID == "n_arrested"):
            if(typeID == "ALL CRIMES"):
                return web.json_response(self.n_arrested.query("SELECT strftime('%Y', date) AS 'year', district, SUM(count) FROM n_arrested group by district, year"))
            else:
                return web.json_response(self.n_arrested.query("SELECT strftime('%Y', date) AS 'year', district, SUM(count) FROM n_arrested WHERE primary_type == \"" + str(typeID) + "\" group by district, year"))