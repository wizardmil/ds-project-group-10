#!/usr/bin/env python3.7.4

import os, json, asyncio
# import database
from webserver import WebServer
from handlers import ApplicationHandler

if __name__ == "__main__":
    # Starting the application handler which
    # contains all route definitions 
    handler = ApplicationHandler()

    # Starting the webserver, passing in all 
    # route definitions
    WebServer(handler.routes, handler.fetcher).run()

