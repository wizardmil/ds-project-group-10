#!/usr/bin/env python3.7.2

from sqlite3 import Error
import time, uuid, sqlite3, json

"""
    uses: pip install pysqlite3
"""

class DatabaseConnection:
    
    # This initializes an SQL Lite database
    def __init__(self, filename:str):
        try:
            self.conn = sqlite3.connect("storage/"+filename+".db")
            self.cur = self.conn.cursor()
            self.filename = filename
        except Error as e:
            print(filename+" Connection Error:")
            print(e) 
    
    def get_all_subscriptions(self):
        self.cur.execute("SELECT * FROM %s"%self.filename)
        data=self.cur.fetchall()
        return data

    # This method is used for creating the table
    # that maintains an overview of all the 
    # creates a table for the strict: ["2001-02-01", 8, "PROSTITUTION", 1]
    def create_metadata_table(self):
        statement = """CREATE TABLE IF NOT EXISTS """+self.filename+""" (
                                    date text NOT NULL,
                                    district text NOT NULL,
                                    primary_type text NOT NULL,
                                    count int NOT NULL
                                );"""
        self.cur.execute(statement)

    def insert(self, date:str, district:str, primary_type:str, count:int):
        update_metadata_statement = """INSERT INTO """+self.filename+""" (date, district, primary_type, count) VALUES (?, ?, ?, ?)"""
        self.cur.execute(update_metadata_statement, (date, district, primary_type, count) )
        self.conn.commit()

    def query(self, query):
        self.cur.execute(query)
        data=self.cur.fetchall()
        return data

    def get_max_date(self):
        statement = """SELECT date FROM """ + self.filename + """ ORDER BY date(date) DESC LIMIT 1"""     
        self.cur.execute(statement)
        data=self.cur.fetchone()

        if data==None:
            return "2001-01-01"
        else:    
            return data[0]