cd src
docker exec datanode1 hdfs dfsadmin -safemode leave
docker build . -t datastreaming:latest 
docker run --rm --ip 172.200.0.240 --hostname pyspark --env-file hadoop.env --network hadoop datastreaming