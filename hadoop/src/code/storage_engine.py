from config import Date
from time import sleep
from threading import Thread
from pyspark.sql.functions import col, max as max_
import asyncio
from pyspark.sql.types import *
from pyspark.sql import SparkSession
from pyspark import SparkConf, SparkContext

class Storage(Thread):
    """
    This is the Storage Engine. 
    
    It follows the pattern:
    1) retrieve greatest stored datetime, maximal
    2) big query for data from maximal + 1 month
    3) check if queried data is newer than what is stored
    4) if yes, store by appending dataframe to data in the HDFS and increase maximal
    5) else, wait 24 hours and redo from 2
    """

    def __init__(self, utility):
        # self.utility can thus be called to access any Spark context
        self.utility = utility
        
        # initialize the Storage class as a thread object (allows for calls such as start)
        Thread.__init__(self)

    def run(self):
        # retrieving the current event loop for asynchronous thread handling (because... python)
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        
        loop.run_until_complete(self.store())  

    async def store(self):
        session = SparkSession.builder.appName("engine").getOrCreate() 
        prev_to = None
        mem = None

        for i in range(2):
            prev_to, memory = self.update(prev_to, session)
            
            if memory!=None:
                mem = memory

            if prev_to.if_now():
                sleep(1*60*60*24) # if the service has retrieved all data that exists, sleep for 24 hours
                prev_to = mem

            sleep(3) # wait 3 seconds for good measure    

    def update(self, prev_to, session):

        if prev_to==None:
            # maximal returns None if nothing has been stored on the HDFS
            maximal = self.get_maximal_date_stored(session)

            # ..and in that case, start from 2001
            frm = Date(2001,1,1) if maximal==None else maximal
        else:
            frm = prev_to  

        # get_next copies frm and increases the day by 1
        to = frm.get_next()

        # query from Google Big Query (using the query generated from the respective datetimes)
        query = self.update_query(frm.to_string(), to.to_string())
        query_job = self.utility.client.query(query)                

        # no reason to continue operating, if columns have been changed
        qdf = query_job.to_dataframe()
        if len(qdf.count()) != 4:
            print("Error in Storage Engine: Fieldstruct must be updated")
            exit(1)
        
        # Creating a Spark dataframe from data retrived.
        df = session.createDataFrame(qdf, schema=self.utility.schema)

        if  df.count() > 0:  
            # if the largest date from the spark dataframe is greater than the maximal stored date
            # then we write it to the HDFS. In any case, this is to make sure data is not duplicated, more than necessary
            if self.get_max_from_dataframe(df).greater_than(frm):

                # filtering such that rows where important values are null, is dropped
                df = df.filter((df.arrest.isNotNull()) & (df.district.isNotNull()) & (df.primary_type.isNotNull()) & (df.date.isNotNull()))

                # arrange data as such
                grouping = ("date","primary_type","arrest","district")

                # create new scheme where 'count' is a dedicated column
                processed_df = session.createDataFrame(df.groupBy(*grouping).count().dropDuplicates().collect(), schema=self.utility.schema_post)
                
                # store in file of correct year and month
                file = frm.to_string().split("-")

                # partition and write to dfs
                processed_df.write.partitionBy(*grouping).mode('append').csv("hdfs://namenode:9000/"+file[0]+"."+file[1]+".txt")

                session.catalog.clearCache()
                return to, to

        session.catalog.clearCache()
        return to, None
        
    def update_query(self, frm, to):
        # constructing the QUERY with the respective timestamps
        query = """SELECT date,primary_type,
                    arrest,district  
                    FROM `bigquery-public-data.chicago_crime.crime`
                    WHERE date BETWEEN """+"'"+frm+"'"+" AND "+"'"+to+"'"         
        return query  

    def get_maximal_date_stored(self, session):
        try:
            file = self.utility.max_processed.to_string().split("-")
            loaded = session.read.csv("hdfs://namenode:9000/"+file[0]+"."+file[1]+".txt", schema=self.utility.schema)  
        except Exception as e:
            # if no data is currently stored, return
            return None
        return self.get_max_from_dataframe(loaded)  


    def get_max_from_dataframe(self, df):
        try:    
            # aggregates the dataframe to locate and return the newest date
            res = Date(None,None,None,str(df.agg(max_(df.date)).collect()[0][0]))
            self.utility.update_max_processed(res)
            return res 
        except:
            print("Something went wrong, when extracting maximal.")
            return None 
