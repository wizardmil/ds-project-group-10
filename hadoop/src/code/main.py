from storage_engine import Storage
from listener import Listener
from transceiver import Transceiver
from config import Utility
from pyspark import SparkConf, SparkContext
import multiprocessing

if __name__ == '__main__':
    print('')
    print('Starting Engine.')

    # init spark configurations
    sparkConfig = SparkConf().setMaster('local').setAppName('Engine')

    # starting shared spark context
    context = SparkContext(conf=sparkConfig)

    # intermediary class
    u = Utility()

    # queue for communication between threads
    queue = multiprocessing.Manager().Queue()

    print('Initializing Components ...')
    storage = Storage(u)
    # listener = Listener(u.config, queue)
    # transceiver = Transceiver(u, queue)

    try:
        storage.start()
        print('    * Storage Engine Started!')
        # listener.start()
        print('    * Listener Started!')
        # transceiver.start()
        print('    * Transceiver Started!')
        print('Engine Running:')
        print('')
    except KeyboardInterrupt:
        print('Service has been shut down.')
        exit(0)