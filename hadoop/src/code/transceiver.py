import asyncio, json
from threading import Thread
from config import Date
import datetime
from kafkaclient.kclient import KafkaClient
from pyspark.sql import SparkSession
from pyspark.mllib.stat import Statistics
from pyspark.sql.types import *
import datetime
import time

class Transceiver(Thread):

    def __init__(self, utility, queue):
        self.config = utility.config
        self.queue = queue
        self.utility = utility

        self.client = KafkaClient(self.config["kafka-address"], self.config["kafka-group-transceiver"])

        self.loop = asyncio.get_event_loop() if asyncio.get_event_loop()!=None else asyncio.get_event_loop()
        asyncio.set_event_loop(self.loop)

        Thread.__init__(self)

    def validate_row(self, row):
        ret = []
        for r in row:
            if isinstance(r, datetime.datetime):
                ret += [r.strftime("%Y-%m-%d")]
            else:
                ret += [str(r)]
        return ret

    def run(self):
        session = SparkSession.builder.appName("listener").getOrCreate()
        while True:
            msg = self.queue.get()

            if msg == None:
                self.queue.task_done()
                break
            
            try:
                query = json.loads(msg)
                query = json.loads(query) 

                if self.utility.max_processed.greater_than(Date(None, None, None, query["to"]))==False:
                    response = {"status":"error", "max-processed":str(self.utility.max_processed.to_string())}
                    asyncio.run(self.client.send_message(self.config['export'], json.dumps(response)))

                elif "SELECT" in list(query.keys()) and "GROUPBY" in list(query.keys()) and "TOPIC" in list(query.keys()):
                    file = str(query["from"]).split("-")


                    df = session.read.csv(file[0]+"."+file[1]+".txt", schema=self.utility.schema_post)
                    df = df.filter((df.date < query["to"]) & (df.date >= query["from"]))

                    df.createOrReplaceTempView("current_query")

                    if "WHERE" in list(query.keys()):
                        sqlDF = session.sql("SELECT "+ query["SELECT"] +" FROM current_query WHERE " + query["WHERE"] + " group by " + query["GROUPBY"])
                    else:    
                        sqlDF = session.sql("SELECT "+ query["SELECT"] +" FROM current_query group by " + query["GROUPBY"])

                    result = {"status":"success"} 
                    result.update({"result":[self.validate_row(row) for row in sqlDF.collect()]})

                    result = self.stat("SUM(count)", query, result, sqlDF)

                    

                    asyncio.run(self.client.send_message(query["TOPIC"], json.dumps(result)))
                    session.catalog.clearCache()
                    session.catalog.dropTempView("current_query")
                else:    
                    file = str(query["from"]).split("-")

                    df = session.read.csv(file[0]+"."+file[1]+".txt", schema=self.utility.schema_post)

                    df = df.filter((df.date < query["to"]) & (df.date >= query["from"]))
                    
                    t = df.filter(df["arrest"] == True)
                    f = df.filter(df["arrest"] == False)

                    result = {"status":"success"}
                    grouping = ("date", "district", "primary_type", "count")

                    result.update({"arrested":[[row["date"].strftime("%Y-%m-%d"), row["district"], row["primary_type"], row["count"]] for row in t.select(*grouping).collect()]})
                    result.update({"not_arrested":[[row["date"].strftime("%Y-%m-%d"), row["district"], row["primary_type"], row["count"]]  for row in f.select(*grouping).collect()]})
                    
                    # result = self.stat("count", query, result, t, f)

                    result.update({"sent":query["time"], "received":str(time.time())})

                    asyncio.run(self.client.send_message(self.config['export'], json.dumps(result)))
                    # session.catalog.clearCache()

            except Exception as err:
                print('Error: In Transciever: %s'%(err))
                response = {"status":str(err)}
                asyncio.run(self.client.send_message(self.config['export'], json.dumps(response)))
            finally:
                self.queue.task_done()
 
        return

    def stat(self, row, query, results, sqlDF, f=None):
        if "STAT" in list(query.keys()):
            rdd = sqlDF.select(row).rdd

            if f!=None:
                frdd = f.select(row).rdd
                fsummary = Statistics.colStats(frdd.map(lambda s: [int(s[0])]))

            summary = Statistics.colStats(rdd.map(lambda s: [int(s[0])]))

            values = str(query["STAT"]).split(",")
            if "variance" in values:
                if f!= None:
                    results.update({"true_variance":str(summary.variance())}) 
                    results.update({"false_variance":str(fsummary.variance())}) 
                else:    
                    results.update({"variance":str(summary.variance())}) 
            if "mean" in values:    
                if f!= None:
                    results.update({"true_mean":str(summary.mean())}) 
                    results.update({"false_mean":str(fsummary.mean())}) 
                else:    
                    results.update({"mean":str(summary.mean())}) 
        return results        