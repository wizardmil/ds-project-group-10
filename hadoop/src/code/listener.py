import asyncio, json
from threading import Thread
from kafkaclient.kclient import KafkaClient, InterfaceResponseHandler
from pyspark.sql.types import Row, StringType
from pyspark.sql import SparkSession

class Listener(Thread, InterfaceResponseHandler):

    def __init__(self, config, queue):
        # declaring utility and message queue vars
        self.queue = queue
        self.config = config

        # starting the Kafka client
        self.client = KafkaClient(self.config["kafka-address"], self.config["kafka-group-listener"])

        # getting an asyncio loop
        self.loop = asyncio.get_event_loop() if asyncio.get_event_loop()!=None else asyncio.new_event_loop()
        asyncio.set_event_loop(self.loop)

        Thread.__init__(self)

    def run(self):
        # starting subscription on "in topic"
        self.loop.run_until_complete(self.client.subscribe(self.config['receive'], self))

    async def handle(self, message):
        self.queue.put(message.value.decode('utf-8'))
