from google.oauth2 import service_account
from pyspark.sql import SQLContext, SparkSession
from pyspark import SparkConf, SparkContext
from google.cloud import bigquery
from pyspark.sql.types import *
import os, json, datetime

# ,StructField("block", StringType(), True)\
# ,StructField("iucr", StringType(), True)\
# ,StructField("description", StringType(), True)\
# ,StructField("location_description", StringType(), True)\
# ,StructField("domestic", BooleanType(), True)\
# ,StructField("beat", IntegerType(), True)\
# StructField("unique_key", LongType(), True)\
# ,StructField("case_number", StringType(), True)\
# ,StructField("ward", FloatType(), True)\
# ,StructField("community_area", StringType(), True)
# ,StructField("fbi_code", StringType(), True)\
# ,StructField("x_coordinate", FloatType(), True)\
# ,StructField("y_coordinate", FloatType(), True)\
# ,StructField("year", IntegerType(), True)
# ,StructField("updated_on", StringType(), True)\
# ,StructField("latitude", DoubleType(), True)\
# ,StructField("longitude", DoubleType(), True)])
# ,StructField("location", StringType(), True)])   

"""
 Intermediary class
 Prodives schemas for pre and post-processing
 Provides the Date class for internal querying
 Provides intermediary state variable (max processed)
 ...and BigQuery Client
"""
class Utility:

    def __init__(self):
        self.path = os.path.dirname(os.path.abspath(__file__))
        self.credentials = service_account.Credentials.from_service_account_file(self.path+"/config/service_key.json")
        self.client = bigquery.Client(credentials=self.credentials, project=self.credentials.project_id)

        with open(self.path+"/config/config.json", "r") as jf:
            self.config = json.load(jf)     

        self.schema = StructType([StructField("date", DateType(), True) ,StructField("primary_type", StringType(), True) ,StructField("arrest", BooleanType(), True) ,StructField("district", StringType(), True)])
        self.schema_post = StructType([StructField("date", DateType(), True),StructField("primary_type", StringType(), True), StructField("arrest", BooleanType(), True), StructField("district", StringType(), True), StructField("count", IntegerType(), True)])                    

        self.max_processed = Date(None, None, None, self.config["max-processed"])

    def update_max_processed(self, date):
        self.max_processed = date
        self.config["max_processed"] = self.max_processed.to_string()

        with open(self.path+"/config/config.json", "w") as jf:
            json.dump(self.config, jf)

class Date:
    def __init__(self, year=None, month=None, day=None, string=None):

        # TODO: optimize design for this constructor
        if year!=None and month!=None and day!=None and string==None:
            self.date = datetime.datetime(int(year), int(month), int(day))
        elif string != None:
            s = string.split("-")  
            self.date = datetime.datetime(int(s[0]), int(s[1]), int(s[2]))

    # Increases the current date one day
    def increase(self):
        self.date += datetime.timedelta(days=1)
        return self    

    # creates a new date based on seld, and increases by one day
    # returns Date
    def get_next(self):
        return Date(int(self.date.year), int(self.date.month), int(self.date.day)).increase()

    # used to compare to Date objects
    def greater_than(self, date):
        if date.date < self.date:
            return True 
        return False

    # formats the date to year-month-day
    def to_string(self):
        return self.date.strftime("%Y-%m-%d")

    # validates if the date is equal or larger than today
    def if_now(self):
        if datetime.datetime.now() <= self.date:
            return True
        return False        
